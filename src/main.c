#define _DEFAULT_SOURCE

#include "mem.h"
#include "mem_internals.h"

#include <assert.h>
#include <unistd.h>

#define OFFSET offsetof(struct block_header, contents)
#define THIRTY_TWO_BYTES 32


typedef enum error error;
enum error
{
    FAIL,
    SUCCESS,

};

error test_basic_alloc()
{
  struct block_header *block = heap_init(1024);

  // printf("block %p\n", block);
  // debug_heap(stderr, block);
  assert(block == HEAP_START);
  assert(block->capacity.bytes == REGION_MIN_SIZE - OFFSET);
  heap_term();

  return SUCCESS;
}

error test_multiple_block_released()
{
   struct block_header* heap = heap_init(THIRTY_TWO_BYTES * 32);
  // printf("block %p\n", block);
  assert(heap);

  void *block_a_data = _malloc(THIRTY_TWO_BYTES);
  void *block_b_data = _malloc(THIRTY_TWO_BYTES);
  struct block_header *block_a = block_a_data - OFFSET;
  assert(block_a->is_free == false);
  // debug_heap(stderr, heap);
  _free(block_a_data);
  assert(block_a->is_free == true);
  // debug_heap(stderr, heap);
  _free(block_b_data);
  assert(block_a->next->capacity.bytes == REGION_MIN_SIZE - THIRTY_TWO_BYTES - 2 * OFFSET);
  // debug_heap(stderr, heap);
  heap_term();

  return SUCCESS;
}

error test_realloc()
{
  struct block_header *heap = heap_init(REGION_MIN_SIZE - OFFSET);

  // printf("block %p\n", block);

  assert(heap);
  // debug_heap(stderr, heap);
  void *block_a_data = _malloc(REGION_MIN_SIZE);
  // debug_heap(stderr, heap);
  struct block_header *block_a = block_a_data - OFFSET;
  assert(block_a->capacity.bytes == REGION_MIN_SIZE);
  assert(block_a->next);
  // printf("block diff %ld", block_a->next - block_a);
  assert((block_a->next = HEAP_START + REGION_MIN_SIZE + OFFSET));
  heap_term();

  return SUCCESS;
}

error test_ext_realloc()
{
  struct block_header *heap = heap_init(REGION_MIN_SIZE - OFFSET);

  // // printf("block %p\n", block);

  assert(heap);

  // debug_heap(stderr, heap);
  void * ptr = mmap((void *)(heap + REGION_MIN_SIZE + OFFSET), 1, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  munmap(ptr, 1);
  void *block_a_data = _malloc(REGION_MIN_SIZE);

  munmap(heap + REGION_MIN_SIZE + OFFSET, 1);
  struct block_header *block_a = block_a_data - OFFSET;
  // debug_heap(stderr, heap);
  // printf("block diff %ld", block_a->next - block_a);
  assert(block_a - heap > 8192);
  // assert(block_a->next);
  // assert(block_a->next = HEAP_START + REGION_MIN_SIZE + OFFSET);
  heap_term();

  return SUCCESS;
}

int main()
{
  if (test_basic_alloc())
    printf("ALLOCATION TEST PASSED\n");
  if (test_multiple_block_released())
    printf("ASSIGN AND RELEASE TEST PASSED\n");
  // int i;
  // scanf("%d", &i);
  if (test_realloc())
    printf("REALLOC SUCCESSFUL\n");

  if (test_ext_realloc())
    printf("REALLOC NOT RIGHT AFTER SUCCESSFUL\n");
  return 0;
}
