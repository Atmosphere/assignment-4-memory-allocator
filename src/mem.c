#define _DEFAULT_SOURCE

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#include <unistd.h>

// #define DEBUG

void debug_block(struct block_header* b, const char* fmt, ...);

void debug(const char* fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header* block){   
  query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
  return block->capacity.bytes >= query; 
}

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
  *((struct block_header *) addr) = (struct block_header) {
          .next = next,
          .capacity = capacity_from_size(block_sz),
          .is_free = true};
}

static size_t region_actual_size(size_t query) { 
  query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
  return size_max(round_pages(query), REGION_MIN_SIZE); 
}

extern inline bool region_is_invalid(const struct region *r);

static bool try_merge_with_next(struct block_header *block);

static void *map_pages(void const *addr, size_t length, int additional_flags) {
  return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

static struct region alloc_region(void const *addr, size_t query) {

  struct region reg = REGION_INVALID;
  int not_wrong_place = 1;

  query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
  size_t size = region_actual_size(query + offsetof(struct block_header, contents));

  void *ptr = map_pages(addr < HEAP_START ? HEAP_START : addr, size, MAP_FIXED_NOREPLACE);

  if ((long int) ptr == -1) {
    ptr = map_pages(addr < HEAP_START ? HEAP_START : addr, size, 0);
    not_wrong_place = 0;
  }

  if ((long int) ptr != -1) {
    reg.addr = ptr;
    reg.size = size;
    reg.extends = not_wrong_place;
    block_init(ptr, (block_size) {.bytes = size}, NULL);
  }

  return reg;
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
  const struct region region = alloc_region(HEAP_START, initial);
  if (region_is_invalid(&region))
    return NULL;

  return region.addr;
}

void munmap_chain(struct block_header *start) { //NOLINT
  while (try_merge_with_next(start));
  if (start->next) {
    munmap_chain(start->next);
  }
  munmap(start, size_from_capacity(start->capacity).bytes);
}

void heap_term() {
  munmap_chain((struct block_header *) HEAP_START);
}

#define BLOCK_MIN_CAPACITY 24


static bool block_splittable(struct block_header *restrict block, size_t query) {
  query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
  return block->is_free &&
         query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
  query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
  if (!block_splittable(block, query))
    return false;
  block_size next_block_size = {block->capacity.bytes - size_max(query, BLOCK_MIN_CAPACITY)};
  block->capacity.bytes = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_header *next_block = block_after(block);
  block_init(next_block, next_block_size, block->next);
  block->next = next_block;
  return true;
}


static void *block_after(struct block_header const *block) {
  return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
  return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
  if (!block || !block->next)
    return false;
  if (!mergeable(block, block->next))
    return false;

  block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
  struct block_header *new_next = block->next->next;
  *(block->next) = (struct block_header) {.next = NULL, .is_free = 0, .capacity = {0}};
  block->next = new_next;

  return true;
}


typedef struct block_search_result block_search_result;
struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK,
        BSR_REACHED_END_NOT_FOUND,
        BSR_CORRUPTED
    } type;
    struct block_header *block;
};

static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
  block_search_result result = {2, block};
  if (!block)
    return result;

  do {
    while (try_merge_with_next(block));
    block = block->next;
  } while (block);

  while ((result.type = !(block_is_big_enough(sz, result.block) && result.block->is_free)) && result.block->next) {
    result.block = result.block->next;
  }
  return result;
}

static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
  query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
  block_search_result result = find_good_or_last(block, query);

  if (!result.type) {
    split_if_too_big(result.block, query);
    result.block->is_free = false;
  }
  return result;
}

static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
  if (!last)
    return NULL;

  struct block_header *heap_end = block_after(last);
  query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
  struct region new_region = alloc_region(heap_end, query);
  if (last->is_free && new_region.extends) {
    last->capacity.bytes += new_region.size;
    return last;
  }
  last->next = new_region.addr;
  return new_region.addr;
}

static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
  query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
  if (!heap_start)
    return heap_init(query);

  block_search_result result = try_memalloc_existing(query, heap_start);

  struct block_header *heap_end = heap_start;
  switch (result.type) {

    case BSR_FOUND_GOOD_BLOCK:
      return result.block;
      break;

    case BSR_REACHED_END_NOT_FOUND:
      while (heap_end->next)
        heap_end = heap_end->next;
      heap_start = grow_heap(heap_end, query);
      result = try_memalloc_existing(query, heap_start);
      return result.block;
      break;

    case BSR_CORRUPTED:
    default:
      return NULL;
      break;
  }
}

void *_malloc(size_t query) {
  query = (query + BLOCK_ALIGN - 1) / BLOCK_ALIGN * BLOCK_ALIGN;
  struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
  if (addr)
    return addr->contents;
  else
    return NULL;
  return NULL;
}

static struct block_header *block_get_header(void *contents) {
  return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
  if (!mem)
    return;
  struct block_header *header = block_get_header(mem);
  header->is_free = true;
  while (try_merge_with_next(header));
}
